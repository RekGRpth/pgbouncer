FROM alpine
RUN set -x \
    && apk add --no-cache --virtual .pgbouncer-rundeps \
        pgbouncer \
        su-exec \
        tzdata \
    && echo done
CMD [ "pgbouncer", "-q", "pgbouncer.ini" ]
ENV HOME=/home
ENTRYPOINT [ "su-exec", "pgbouncer" ]
WORKDIR "${HOME}"
