#!/bin/sh -x

#docker build --tag rekgrpth/pgbouncer . || exit $?
#docker push rekgrpth/pgbouncer || exit $?
docker pull rekgrpth/pgbouncer || exit $?
docker network create --attachable --driver overlay docker || echo $?
docker service rm pgbouncer || echo $?
docker service create \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --hostname tasks.pgbouncer \
    --name pgbouncer \
    --network name=docker \
    --mount type=bind,source=/var/lib/docker/volumes/repmgr/_data/pg_data/pgbouncer.ini,destination=/home/pgbouncer.ini,readonly \
    --mount type=bind,source=/var/lib/docker/volumes/repmgr/_data/pg_data/pg_hba.conf,destination=/home/pg_hba.conf,readonly \
    rekgrpth/pgbouncer
